package com.example.project

import cats.Id
import org.scalatest.flatspec._
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper

class HelloWorldSpec extends AnyFlatSpec {
  "Method 'hello'" should "write a string 'Hello World!' on server" in {
    val helloWorld = HelloWorld.impl[Id]
    helloWorld.hello().greeting shouldBe "Hello World!"
  }
}
