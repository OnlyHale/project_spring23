package com.example.project

import cats.Applicative
import cats.implicits._

trait HelloWorld[F[_]] {
  def hello(): F[HelloWorld.Greeting]
}

object HelloWorld {
  final case class Greeting(greeting: String) extends AnyVal

  def impl[F[_]: Applicative]: HelloWorld[F] = new HelloWorld[F] {
    def hello(): F[HelloWorld.Greeting] =
      Greeting("Hello World!").pure[F]
  }
}
