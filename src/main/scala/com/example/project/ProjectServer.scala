package com.example.project

import cats.effect.Async
import cats.syntax.all._
import com.comcast.ip4s._
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.implicits._
import org.http4s.server.middleware.Logger
import sttp.tapir.server.http4s.Http4sServerInterpreter
import sttp.tapir.swagger.bundle.SwaggerInterpreter

object ProjectServer {

  def run[F[_]: Async]: F[Nothing] = {

    val helloWorldAlg = HelloWorld.impl[F]
    val swagger = SwaggerInterpreter().fromEndpoints[F](
      List(ProjectRoutes.hello),
      "My App",
      "1.0"
    )
    val swaggerRoute = Http4sServerInterpreter[F]().toRoutes(swagger)
    val httpApp = (
      ProjectRoutes.helloWorldRoutes[F](helloWorldAlg) <+>
        swaggerRoute
    ).orNotFound

    // With Middlewares in place
    val finalHttpApp = Logger.httpApp(true, true)(httpApp)
    for {
      _ <- EmberServerBuilder
        .default[F]
        .withHost(ipv4"0.0.0.0")
        .withPort(port"8080")
        .withHttpApp(finalHttpApp)
        .build
    } yield ()
  }.useForever
}
