package com.example.project

import cats.effect.{IOApp, IO}

object Main extends IOApp.Simple {
  val run: IO[Nothing] = ProjectServer.run[IO]
}
