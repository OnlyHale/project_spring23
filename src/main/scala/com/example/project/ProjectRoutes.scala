package com.example.project

import cats.effect.Async
import cats.implicits._
import com.example.project.HelloWorld.Greeting
import org.http4s.HttpRoutes
import sttp.tapir._
import sttp.tapir.server.http4s.Http4sServerInterpreter

object ProjectRoutes {
// Кол базового проекта на тапире
  val hello = endpoint.get
    .in("hello")
    .errorOut(plainBody[String])
    .out(plainBody[Greeting])

  def helloWorldRoutes[F[_]: Async](helloWorld: HelloWorld[F]): HttpRoutes[F] = {
    Http4sServerInterpreter[F]().toRoutes(
      hello
        .serverLogic(_ =>
          helloWorld
            .hello()
            .attempt
            .map(_.leftMap(_.getMessage()))
        )
    )
  }
}
