lazy val root = (project in file("."))
  .settings(
    organization := "com.example",
    name := "project",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.13.10",
    libraryDependencies ++= Seq(
      "org.http4s"                  %% "http4s-ember-server"     % "0.23.18",
      "ch.qos.logback"              % "logback-classic"          % "1.4.6" % Runtime,
      "com.softwaremill.sttp.tapir" %% "tapir-http4s-server"     % "1.2.10",
      "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % "1.2.10",
      "org.scalatest"               %% "scalatest"               % "3.2.15" % "test"
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.13.2" cross CrossVersion.full),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
    testFrameworks += new TestFramework("munit.Framework")
  )
